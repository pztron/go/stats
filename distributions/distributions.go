/*
* A part of my "stats" library that attempts to replicate the function (but not necessarily form) of the normalPdf, 
	normalCdf, inverseNormal, tPdf, tCdf, inverseT, binomialPdf, binomialCdf, inverseBinomial, 
	geometricPdf, and geometricCdf functions of the ti-nspire calculator. 

*/
package main
import (
	"fmt"
	"math"
)



func normalize(x, std, mean float32) float32 {
	return ((x - mean) / std)

}

// algebraic for now - use calculus later

func normalPdf(x, std, mean float64) {
	fmt.Printf("%f\n", ((1/(std*math.Sqrt(2*math.Pi))) * math.Exp((-.5*math.Pow(((x-mean)/std),2)))))
	fmt.Printf("%f\n", (math.Exp(math.Log((1/(std*math.Sqrt(2.0*math.Pi))))-(.5*math.Pow(((x-mean)/std),2)))))
}

func main() {
	normalPdf(.1,1,0)
}